<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LessonController extends Controller
{
    public function index()
    {
        return 'Lesson index';
    }

    public function pro()
    {
        return 'Pro lesson index';
    }
}
