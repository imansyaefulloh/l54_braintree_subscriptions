<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function index(Request $request)
    {
        return view('invoices.index')->with([
            'invoices' => $request->user()->invoices(),
        ]);
    }

    public function download(Request $request, $invoiceId)
    {
        return $request->user()->downloadInvoice($invoiceId, [
            'vendor' => 'Iman Saefulloh Ltd',
            'product' => 'Some produts',
        ]);
    }
}
