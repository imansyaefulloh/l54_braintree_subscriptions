@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Subscriptions</div>

                <div class="panel-body">
                    @if (Auth::user()->subscription('main')->cancelled())
                        <p>You have canceled your subscription. You still have access until {{ Auth::user()->subscription('main')->ends_at->format('dS M Y') }}</p>
                        <form action="{{ route('subscription.resume') }}" method="post">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-default">Resume subscription</button>
                        </form>
                    @else
                        <p>You are currently subscribed!</p>
                        <form action="{{ route('subscription.cancel') }}" method="post">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-default">Cancel subscription</button>
                        </form>
                    @endif
                    <hr>
                    <a href="{{ route('plans.index')}}">Change plan</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
