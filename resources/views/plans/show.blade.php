@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{ ucfirst($plan->name) }}</div>

                <div class="panel-body">
                    <form action="{{ route('subscription.create') }}" method="post">
                        {{ csrf_field() }}
                        <div id="dropin-container"></div>
                        <hr>
                        <input type="hidden" name="plan" value="{{ $plan->id }}">
                        <button type="submit" class="btn btn-default hidden" id="payment-button">Pay</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://js.braintreegateway.com/js/braintree-2.31.0.min.js"></script>
<script>
    $.ajax({
        type: 'GET',
        url: '{{ route('braintree.token') }}',
        success: function(response) {
            braintree.setup(response.data.token, 'dropin', {
                container: 'dropin-container',
                onReady: function() {
                    $('#payment-button').removeClass('hidden');
                }
            });
        }
    });
</script>
@endsection